var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/', function(req, res) {
    var db = req.db;
    console.log('Admin Mounted');
    db.collection('roomcollection').find().toArray(function (err, items) {
        res.json(items);
    });
});

module.exports = router;
